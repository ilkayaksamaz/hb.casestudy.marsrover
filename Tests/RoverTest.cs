﻿using System.Collections.Generic;
using MarsRover.Infrastructure;
using MarsRover.Infrastructure.Model.Rover;
using MarsRover.Infrastructure.Model.Surface;
using Moq;
using NUnit.Framework;

namespace Tests
{
    public class RoverTest 
    {
        [TestFixture]
        public class RoverConstructor
        {
            [Test]
            [TestCase(1, 2, Direction.E)]
            [TestCase(3, 3, Direction.N)]
            public void initialize_rover_with_point_and_direction(int x, int y, Direction direction)
            {
                var point = new Point(x, y);
                var rover = new Rover(point, direction);

                Assert.AreEqual(rover.GetCurrentPosition(), point);
                Assert.AreEqual(rover.GetCurrentDirection(), direction);
            }
        }

        [TestFixture]
        public class RoverMovement
        {
            private static readonly object[] _sourceLists =
            {
                new object[]
                {
                    1,
                    2,
                    Direction.N,
                    new List<Movement>
                    {
                        Movement.L, Movement.M, Movement.L, Movement.M, Movement.L, Movement.M, Movement.L, Movement.M,
                        Movement.M
                    },
                    1,
                    3,
                    Direction.N
                },
                new object[]
                {
                    3,
                    3,
                    Direction.E,
                    new List<Movement>
                    {
                        Movement.M, Movement.M, Movement.R, Movement.M, Movement.M, Movement.R, Movement.M, Movement.R,
                        Movement.R, Movement.M
                    },
                    5,
                    1,
                    Direction.E
                }
            };

            public Mock<IPlateau> Plateau { get; set; }

            [SetUp]
            public void SetUp()
            {
                Plateau = new Mock<IPlateau>();
            }

            [Test]
            [TestCaseSource("_sourceLists")]
            public void rover_execute_movement_commands(int x, int y, Direction direction,
                IEnumerable<Movement> commands, int cX, int cY, Direction cDirection)
            {
                var point = new Point(x, y);

                Plateau.Setup(p => p.IsValid(It.IsAny<Point>())).Returns(true);
                var rover = new Rover(point, direction);
                rover.Move(commands, Plateau.Object);

                var roverPosition = rover.GetCurrentPosition();
                var roverDirection = rover.GetCurrentDirection();
                
                Assert.AreEqual(roverPosition.X, cX);
                Assert.AreEqual(roverPosition.Y, cY);
                Assert.AreEqual(roverDirection, cDirection);
            }
        }
    }
}