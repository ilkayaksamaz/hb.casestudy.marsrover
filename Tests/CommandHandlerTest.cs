﻿using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using MarsRover.Infrastructure;
using MarsRover.Infrastructure.Command;
using MarsRover.Infrastructure.CommandHandler;
using MarsRover.Infrastructure.Model.Rover;
using MarsRover.Infrastructure.Model.Surface;
using Moq;
using NUnit.Framework;
using Point = MarsRover.Infrastructure.Model.Rover.Point;

namespace Tests
{
    public class CommandHandlerTest
    {
        [TestFixture]
        public class SetLandingSurfaceCommandHandlerTest
        {
            public Mock<IPlateau> Plateau { get; set; }

            [SetUp]
            public void SetUp()
            {
                Plateau = new Mock<IPlateau>();
            }

            [Test]
            public void set_landing_surface_command_handle()
            {
                var w = 5;
                var h = 5;
                var commandHandler = new SetLandingSurfaceSizeCommandHandler(Plateau.Object);
                commandHandler.Handle(new SetLandingSurfaceSizeCommand(5, 5), CancellationToken.None);

                Plateau.Verify(p => p.SetSize(w, h), Times.Once);
            }

            [Test, TestCase(5, 5)]
            public void set_landing_surface_command_handle_are_inputs_equals_to_plateau_size(int w, int h)
            {
                var plateau = new Plateau();
                var commandHandler = new SetLandingSurfaceSizeCommandHandler(plateau);
                commandHandler.Handle(new SetLandingSurfaceSizeCommand(w, h), CancellationToken.None);


                var size = plateau.GetSize();
                Assert.AreEqual(w, size.w);
                Assert.AreEqual(h, size.h);
            }
        }


        [TestFixture]
        public class SetRoverCurrentPositionCommandHandlerTest
        {
            public Mock<IPlateau> Plateau { get; set; }

            [SetUp]
            public void SetUp()
            {
                Plateau = new Mock<IPlateau>();
            }

            [Test]
            public void set_landing_surface_command_handle()
            {
                var x = 1;
                var y = 2;
                var d = Direction.N;

                var commandHandler = new SetRoverCurrentPositionCommandHandler(Plateau.Object);
                commandHandler.Handle(new SetRoverCurrentPositionCommand(x, y, d), CancellationToken.None);

                Plateau.Verify(p => p.AddRover(It.IsAny<IRover>()), Times.Once);
            }

            [Test, TestCase(1, 2, Direction.N)]
            public void set_landing_surface_command_handle_are_inputs_equals_to_rover_position(int x, int y,
                Direction d)
            {
                var plateau = new Plateau();
                plateau.SetSize(5, 5);
                var commandHandler = new SetRoverCurrentPositionCommandHandler(plateau);
                commandHandler.Handle(new SetRoverCurrentPositionCommand(x, y, d), CancellationToken.None);

                var lastRover = plateau.GetLastRover();
                Assert.IsNotNull(lastRover);

                var position = lastRover.GetCurrentPosition();
                var direction = lastRover.GetCurrentDirection();

                Assert.IsNotNull(position);
                Assert.IsNotNull(direction);
                Assert.AreEqual(x, position.X);
                Assert.AreEqual(y, position.Y);
                Assert.AreEqual(d, direction);
            }
        }

        public class MovementCommandHandlerTest
        {
            public Mock<IPlateau> Plateau { get; set; }

            [SetUp]
            public void SetUp()
            {
                Plateau = new Mock<IPlateau>();
            }

            [Test]
            public void set_movement_command_handle()
            {
                Plateau.Setup(p => p.GetLastRover()).Returns(new Rover(new Point(1, 2), Direction.E));
                Plateau.Setup(p => p.GetLastRover().Move(It.IsAny<IEnumerable<Movement>>(), Plateau.Object));

                var movementCommands = new List<Movement>();
                var commandHandler = new MovementCommandHandler(Plateau.Object);
                commandHandler.Handle(new MovementCommand(movementCommands), CancellationToken.None);

                Plateau.Verify(p => p.GetLastRover(), Times.Once);
                Plateau.Verify(p => p.GetLastRover().Move(It.IsAny<IEnumerable<Movement>>(), Plateau.Object),
                    Times.Once);
            }

            private static readonly object[] _sourceLists =
            {
                new object[]
                {
                    1,
                    2,
                    Direction.N,
                    new List<Movement>
                    {
                        Movement.L, Movement.M, Movement.L, Movement.M, Movement.L, Movement.M, Movement.L, Movement.M,
                        Movement.M
                    },
                    1,
                    3,
                    Direction.N
                },
                new object[]
                {
                    3,
                    3,
                    Direction.E,
                    new List<Movement>
                    {
                        Movement.M, Movement.M, Movement.R, Movement.M, Movement.M, Movement.R, Movement.M, Movement.R,
                        Movement.R, Movement.M
                    },
                    5,
                    1,
                    Direction.E
                }
            };

            [Test]
            [TestCaseSource("_sourceLists")]
            public void set_movement_command_handle_are_inputs_equals_to_rover_position(int x, int y,
                Direction direction,
                IEnumerable<Movement> commands, int cX, int cY, Direction cDirection)
            {
                var plateau = new Plateau();
                plateau.SetSize(5, 5);
                plateau.AddRover(new Rover(new Point(x, y), direction));

                
                var commandHandler = new MovementCommandHandler(plateau);
                commandHandler.Handle(new MovementCommand(commands), CancellationToken.None);

                var lastRover = plateau.GetLastRover();
                Assert.IsNotNull(lastRover);

                var currentPosition = lastRover.GetCurrentPosition();
                var currentDirection = lastRover.GetCurrentDirection();

                Assert.IsNotNull(currentPosition);
                Assert.IsNotNull(currentDirection);
                Assert.AreEqual(cX, currentPosition.X);
                Assert.AreEqual(cY, currentPosition.Y);
                Assert.AreEqual(cDirection, currentDirection);
            }
        }
    }
}