﻿using MarsRover.Infrastructure;
using MarsRover.Infrastructure.Command.Exceptions;
using MarsRover.Infrastructure.Model.Rover;
using MarsRover.Infrastructure.Model.Surface;
using Moq;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class PlateauTest
    {
        [TestFixture]
        public class PlateauSize
        {
            [Test()]
            [TestCase(5, 5)]
            [TestCase(10, 10)]
            public void plateau_set_size(int width, int height)
            {
                var plateau = new Plateau();
                plateau.SetSize(width, height);


                var (w, h) = plateau.GetSize();
                Assert.AreEqual(h, h);
                Assert.AreEqual(w, w);
            }
        }

        [TestFixture]
        public class PlateauPoint
        {
            [Test()]
            [TestCase(5, 5, 2, 4)]
            [TestCase(10, 10, 9, 8)]
            public void set_size_and_check_point_in_boundaries_returns_true(int plateauW, int plateauH, int pointX,
                int pointY)
            {
                var plateau = new Plateau();
                plateau.SetSize(plateauW, plateauH);
                var point = new Point(pointX, pointY);
                var isValid = plateau.IsValid(point);
                Assert.That(isValid);
            }

            [Test()]
            [TestCase(5, 5, 2, -4)]
            [TestCase(10, 10, 9, 11)]
            public void set_size_and_check_point_in_boundaries_returns_false(int plateauW, int plateauH, int pointX,
                int pointY)
            {
                var plateau = new Plateau();
                plateau.SetSize(plateauW, plateauH);
                var point = new Point(pointX, pointY);
                var isValid = plateau.IsValid(point);
                Assert.That(!isValid);
            }
        }

        [TestFixture]
        public class PlateauRover
        {
            [Test]
            public void add_rover_to_plateau_get_last_added()
            {
                var plateau = new Plateau();
                plateau.SetSize(5, 5);

                const Direction direction = Direction.E;
                var roverPosition = new Point(1, 2);
                var rover = new Rover(roverPosition, direction);
                plateau.AddRover(rover);
                var lastRover = plateau.GetLastRover();
                Assert.AreEqual(rover, lastRover);
            }

            [Test]
            [TestCase(1, 2)]
            public void add_rover_to_plateau_already_rover_landed_position_expect_exception(int roverX, int roverY)
            {
                var plateau = new Plateau();
                plateau.SetSize(5, 5);

                const Direction direction = Direction.E;
                var roverPosition = new Point(1, 2);
                var rover = new Rover(roverPosition, direction);
                plateau.AddRover(rover);
                Assert.Throws<InvalidLandingLocation>(delegate {  plateau.AddRover(rover); });
            }
        }
    }
}