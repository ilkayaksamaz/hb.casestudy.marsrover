﻿using System;
using System.Linq;
using System.Text;
using MarsRover.Infrastructure.Command;
using MarsRover.Infrastructure.Command.Parsers;
using NUnit.Framework;

namespace Tests
{
    public class CommandParserTest
    {
        [TestFixture]
        public class Parse
        {
            private static readonly object[] TestCaseInput = new []{ "5 5 \r\n 1 2 N \r\n LMLMLMLMM \r\n 3 3 E \r\n MMRMMRMRRM"};

            [Test, TestCaseSource("TestCaseInput")]
            public void parse_command_return_expected_size(string input)
            {
                var commandParser = new CommandParser();
                var commands = commandParser.Parse(input);
                Assert.AreEqual(5,commands.Count());
            }
          
            [Test, TestCaseSource("TestCaseInput")]
            public void first_command_must_be_set_landing_surface_command(string input)
            {
                var commandParser = new CommandParser();
                var commands = commandParser.Parse(input);
                Assert.IsAssignableFrom<SetLandingSurfaceSizeCommand>(commands.First());
            }
            
            [Test, TestCaseSource("TestCaseInput")]
            public void all_odd_indexed_items_in_command_list_must_be_rover_position_commands(string input)
            {
                var commandParser = new CommandParser();
                var commands = commandParser.Parse(input);
                var oddCommands  = commands.Where((c,i) => i % 2 != 0);
                Assert.That(oddCommands, Is.All.AssignableFrom<SetRoverCurrentPositionCommand>());
            }
            
            
            [Test, TestCaseSource("TestCaseInput")]
            public void all_even_indexed_items_in_command_list_must_be_movement_commands(string input)
            {
                var commandParser = new CommandParser();
                var commands = commandParser.Parse(input);
                var evenCommands  = commands.Where((c,i) => i % 2 == 0 && i != 0);
                Assert.That(evenCommands, Is.All.AssignableFrom<MovementCommand>());
            }
        }
    }
}