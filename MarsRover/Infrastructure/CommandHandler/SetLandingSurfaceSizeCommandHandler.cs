﻿using System.Threading;
using System.Threading.Tasks;
using MarsRover.Infrastructure.Command;
using MarsRover.Infrastructure.Model.Surface;
using MediatR;

namespace MarsRover.Infrastructure.CommandHandler
{
    public class SetLandingSurfaceSizeCommandHandler : ICommandHandler<SetLandingSurfaceSizeCommand>
    {
        private readonly IPlateau _plateau;

        public SetLandingSurfaceSizeCommandHandler(IPlateau plateau)
        {
            _plateau = plateau;
        }

        public Task<Unit> Handle(SetLandingSurfaceSizeCommand request, CancellationToken cancellationToken)
        {
            _plateau.SetSize(request.Width, request.Height);
            return Task.FromResult(Unit.Value);
        }
    }
}