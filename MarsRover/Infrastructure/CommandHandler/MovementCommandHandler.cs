﻿using System.Threading;
using System.Threading.Tasks;
using MarsRover.Infrastructure.Command;
using MarsRover.Infrastructure.Model.Surface;
using MediatR;

namespace MarsRover.Infrastructure.CommandHandler
{
    public class MovementCommandHandler : ICommandHandler<MovementCommand>
    {
        private readonly IPlateau _plateau;

        public MovementCommandHandler(IPlateau plateau)
        {
            _plateau = plateau;
        }

        public Task<Unit> Handle(MovementCommand request, CancellationToken cancellationToken)
        {
            var lastRover = _plateau.GetLastRover();
            lastRover.Move(request.Movements, _plateau);
            return Task.FromResult(Unit.Value);
        }
    }
}