﻿using System.Threading;
using System.Threading.Tasks;
using MarsRover.Infrastructure.Command;
using MarsRover.Infrastructure.Model.Rover;
using MarsRover.Infrastructure.Model.Surface;
using MediatR;

namespace MarsRover.Infrastructure.CommandHandler
{
    public class SetRoverCurrentPositionCommandHandler : ICommandHandler<SetRoverCurrentPositionCommand>
    {
        private readonly IPlateau _plateau;

        public SetRoverCurrentPositionCommandHandler(IPlateau plateau)
        {
            _plateau = plateau;
        }

        public Task<Unit> Handle(SetRoverCurrentPositionCommand request, CancellationToken cancellationToken)
        {
            var rover = new Rover(new Point(request.X, request.Y), request.Direction);
            _plateau.AddRover(rover);
            return Task.FromResult(Unit.Value);
        }
    }
}