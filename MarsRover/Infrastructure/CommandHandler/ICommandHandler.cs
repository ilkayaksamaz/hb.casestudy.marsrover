﻿using MediatR;

namespace MarsRover.Infrastructure.CommandHandler
{
    public interface ICommandHandler<in TCommand> : IRequestHandler<TCommand>
        where TCommand : IRequest<Unit>
    {
    }
}