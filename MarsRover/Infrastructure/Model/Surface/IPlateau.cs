﻿using MarsRover.Infrastructure.Model.Rover;

namespace MarsRover.Infrastructure.Model.Surface
{
    public interface IPlateau
    {
        bool IsValid(Point point);
        IRover GetLastRover();
        void AddRover(IRover rover);
        void SetSize(int w, int h);
        (int w, int h) GetSize();
    }
}