﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using MarsRover.Infrastructure.Command.Exceptions;
using MarsRover.Infrastructure.Model.Rover;

namespace MarsRover.Infrastructure.Model.Surface
{
    public class Plateau : IPlateau
    {
        private readonly IList<IRover> _rovers;

        public Plateau()
        {
            _rovers = new List<IRover>();
        }

        public int Width { get; private set; }
        public int Height { get; private set; }

        public bool IsValid(Point point)
        {
            var widthControl = point.X <= Width && point.X >= 0;
            var heightControl = point.Y <= Height && point.Y >= 0;
            var roverControl = _rovers.Any(p => Equals(p.GetCurrentPosition(), point));

            return widthControl && heightControl && !roverControl;
        }

        public IRover GetLastRover()
        {
            return _rovers.LastOrDefault();
        }

        public void AddRover(IRover rover)
        {
            if (!IsValid(rover.GetCurrentPosition()))
                throw new InvalidLandingLocation();

            _rovers.Add(rover);
        }

        public void SetSize(int w, int h)
        {
            Height = h;
            Width = w;
        }

        public (int w, int h) GetSize()
        {
            return (Width, Height);
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            foreach (var rover in _rovers) stringBuilder.AppendLine(rover.ToString());

            return stringBuilder.ToString();
        }
    }
}