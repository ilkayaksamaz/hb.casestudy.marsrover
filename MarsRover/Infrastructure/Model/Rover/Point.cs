﻿using System;

namespace MarsRover.Infrastructure.Model.Rover
{
    public class Point
    {
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; private set; }
        public int Y { get; private set; }

        public void Set(Point point)
        {
            X = point.X;
            Y = point.Y;
        }

        public void SetForwardX()
        {
            X = X + 1;
        }

        public void SetForwardY()
        {
            Y = Y + 1;
        }

        public void SetBackwardX()
        {
            X = X - 1;
        }

        public void SetBackwardY()
        {
            Y = Y - 1;
        }

        public override string ToString()
        {
            return $"{X} - {Y}";
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, null))
                return false;

            if (!(obj is Point point))
                return false;

            return Equals(point);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y);
        }

        private bool Equals(Point other)
        {
            return X == other.X && Y == other.Y;
        }
    }
}