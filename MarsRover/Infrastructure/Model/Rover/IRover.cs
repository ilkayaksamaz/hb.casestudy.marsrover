﻿using System.Collections.Generic;
using MarsRover.Infrastructure.Model.Surface;

namespace MarsRover.Infrastructure.Model.Rover
{
    public interface IRover
    {
        Point GetCurrentPosition();
        Direction GetCurrentDirection();
        void Move(IEnumerable<Movement> movements, IPlateau plateau);
    }
}