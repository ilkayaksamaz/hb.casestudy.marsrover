﻿using System;
using System.Collections.Generic;
using MarsRover.Infrastructure.Model.Surface;

namespace MarsRover.Infrastructure.Model.Rover
{
    public class Rover : IRover
    {
        private Direction _direction;
        private Point _point;

        public Rover(Point point, Direction direction)
        {
            _point = point;
            _direction = direction;
        }


        public Point GetCurrentPosition()
        {
            return _point;
        }

        public Direction GetCurrentDirection()
        {
            return _direction;
        }

        public void Move(IEnumerable<Movement> movements, IPlateau plateau)
        {
            foreach (var movement in movements)
                switch (movement)
                {
                    case Movement.L:
                        MoveLeft();
                        break;
                    case Movement.M:
                        var nextPosition = Move();
                        SetRoverPosition(plateau, nextPosition);
                        break;
                    case Movement.R:
                        MoveRight();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
        }

        private void SetRoverPosition(IPlateau plateau, Point nextPosition)
        {
            if (plateau.IsValid(nextPosition))
                _point = nextPosition;
        }

        private Point Move()
        {
            var newPoint = new Point(_point.X, _point.Y);
            switch (_direction)
            {
                case Direction.N:
                    newPoint.SetForwardY();
                    break;
                case Direction.E:
                    newPoint.SetForwardX();
                    break;
                case Direction.S:
                    newPoint.SetBackwardY();
                    break;
                case Direction.W:
                    newPoint.SetBackwardX();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return newPoint;
        }

        private void MoveRight()
        {
            switch (_direction)
            {
                case Direction.N:
                    _direction = Direction.E;
                    break;
                case Direction.E:
                    _direction = Direction.S;
                    break;
                case Direction.S:
                    _direction = Direction.W;
                    break;
                case Direction.W:
                    _direction = Direction.N;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void MoveLeft()
        {
            switch (_direction)
            {
                case Direction.N:
                    _direction = Direction.W;
                    break;
                case Direction.E:
                    _direction = Direction.N;
                    break;
                case Direction.S:
                    _direction = Direction.E;
                    break;
                case Direction.W:
                    _direction = Direction.S;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override string ToString()
        {
            return $"{_point} - {_direction.ToString()}";
        }
    }
}