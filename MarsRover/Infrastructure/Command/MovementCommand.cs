﻿using System.Collections.Generic;

namespace MarsRover.Infrastructure.Command
{
    public class MovementCommand : ICommand
    {
        public readonly IEnumerable<Movement> Movements;

        public MovementCommand(IEnumerable<Movement> movements)
        {
            Movements = movements;
        }
    }
}