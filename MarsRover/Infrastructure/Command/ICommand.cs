﻿using MediatR;

namespace MarsRover.Infrastructure.Command
{
    public interface ICommand : IRequest
    {
    }
}