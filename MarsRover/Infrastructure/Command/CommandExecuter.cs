﻿using System.Threading.Tasks;
using MarsRover.Infrastructure.Command.Parsers;
using MediatR;

namespace MarsRover.Infrastructure.Command
{
    public class CommandExecutor : ICommandExecutor
    {
        private readonly ICommandParser _commandParser;
        private readonly IMediator _mediator;

        public CommandExecutor(ICommandParser commandParser, IMediator mediator)
        {
            _commandParser = commandParser;
            _mediator = mediator;
        }

        public async Task Execute(string commands)
        {
            var populatedCommands = _commandParser.Parse(commands);
            foreach (var command in populatedCommands)
                await _mediator.Send(command).ConfigureAwait(false);
        }
    }
}