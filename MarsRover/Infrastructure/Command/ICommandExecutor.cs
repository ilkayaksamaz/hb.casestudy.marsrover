﻿using System.Threading.Tasks;

namespace MarsRover.Infrastructure.Command
{
    public interface ICommandExecutor
    {
        Task Execute(string commands);
    }
}