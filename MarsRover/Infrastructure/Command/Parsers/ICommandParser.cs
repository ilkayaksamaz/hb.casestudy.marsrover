﻿using System.Collections.Generic;

namespace MarsRover.Infrastructure.Command.Parsers
{
    public interface ICommandParser
    {
        IEnumerable<ICommand> Parse(string input);
    }
}