﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MarsRover.Infrastructure.Command.Parsers
{
    public class CommandParser : ICommandParser
    {
        public IEnumerable<ICommand> Parse(string input)
        {
            var commandList = input
                .Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);

            for (var i = 0; i < commandList.Length; i++)
            {
                var command = commandList[i];

                switch (i)
                {
                    case 0:
                        var (surfaceX, surfaceY) = ParseSurfaceCommand(command);
                        yield return new SetLandingSurfaceSizeCommand(surfaceX, surfaceY);
                        break;
                    case int n when n % 2 == 1:
                        var (pX, pY, pD) = ParseCurrentPositionCommand(command);
                        yield return new SetRoverCurrentPositionCommand(pX, pY, pD);
                        break;
                    case int n when n % 2 == 0:
                        var movements = ParseMovementCommand(command);
                        yield return new MovementCommand(movements);
                        break;
                }
            }
        }

        private static (int x, int y) ParseSurfaceCommand(string input)
        {
            var commands = input.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            var x = int.Parse(commands[0]);
            var y = int.Parse(commands[1]);

            return (x, y);
        }

        private static (int x, int y, Direction d) ParseCurrentPositionCommand(string input)
        {
            var commands = input.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            var x = int.Parse(commands[0]);
            var y = int.Parse(commands[1]);
            var d = (Direction) Enum.Parse(typeof(Direction), commands[2]);
            return (x, y, d);
        }

        private static IEnumerable<Movement> ParseMovementCommand(string input)
        {
            return input.Select(p => (Movement) Enum.Parse(typeof(Movement), p.ToString()));
        }
    }
}