﻿using System;

namespace MarsRover.Infrastructure.Command.Exceptions
{
    public class InvalidLandingLocation : Exception
    {
        public InvalidLandingLocation()
            : base("Already landed a rover into plateau with given coordinates")
        {
        }
    }
}