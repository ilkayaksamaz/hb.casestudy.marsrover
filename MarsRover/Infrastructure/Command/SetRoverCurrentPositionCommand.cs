﻿namespace MarsRover.Infrastructure.Command
{
    public class SetRoverCurrentPositionCommand : ICommand
    {
        public readonly Direction Direction;
        public readonly int X;
        public readonly int Y;

        public SetRoverCurrentPositionCommand(int x, int y, Direction direction)
        {
            X = x;
            Y = y;
            Direction = direction;
        }
    }
}