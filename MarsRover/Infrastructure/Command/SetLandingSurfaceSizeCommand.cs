﻿namespace MarsRover.Infrastructure.Command
{
    public class SetLandingSurfaceSizeCommand : ICommand
    {
        public SetLandingSurfaceSizeCommand(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public int Width { get; }
        public int Height { get; }
    }
}