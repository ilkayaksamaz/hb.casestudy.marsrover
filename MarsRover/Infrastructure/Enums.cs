﻿namespace MarsRover.Infrastructure
{
    public enum Movement
    {
        L,
        M,
        R
    }

    public enum Direction
    {
        N,
        E,
        S,
        W
    }
}