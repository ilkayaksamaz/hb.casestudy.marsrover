﻿using System;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using MarsRover.Infrastructure.Command;
using MarsRover.Infrastructure.Command.Parsers;
using MarsRover.Infrastructure.Model.Surface;
using MediatR.SimpleInjector;
using SimpleInjector;

namespace MarsRover
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            string commandStr;
#if DEBUG
            commandStr = BuildCommands();
#else
            commandStr = RetrieveCommands();
#endif

            var assembly = Assembly.GetExecutingAssembly();

            var container = new Container();
            container.BuildMediator(assembly);

            container.Register<IPlateau, Plateau>(Lifestyle.Singleton);
            container.Register<ICommandExecutor, CommandExecutor>(Lifestyle.Singleton);
            container.Register<ICommandParser, CommandParser>(Lifestyle.Singleton);

            container.Verify();

            var commandExecutor = container.GetInstance<ICommandExecutor>();
            await commandExecutor.Execute(commandStr).ConfigureAwait(false);
            var plateau = container.GetInstance<IPlateau>();
            var status = plateau.ToString();
            Console.WriteLine(status);

            Console.ReadLine();
        }


        private static string BuildCommands()
        {
            var commandStringBuilder = new StringBuilder();
            commandStringBuilder.AppendLine("5 5");
            commandStringBuilder.AppendLine("1 2 N");
            commandStringBuilder.AppendLine("LMLMLMLMM");
            commandStringBuilder.AppendLine("3 3 E");
            commandStringBuilder.Append("MMRMMRMRRM");
            return commandStringBuilder.ToString();
        }

        private static string RetrieveCommands()
        {
            var commandStringBuilder = new StringBuilder();
            var i = 0;
            while (i < 5)
            {
                switch (i)
                {
                    case 0:
                        Console.WriteLine("Enter surface size:");
                        break;
                    case int n when n % 2 == 1:
                        Console.WriteLine("Enter rover landing coordinates:");
                        break;
                    case int n when n % 2 == 0:
                        Console.WriteLine("Enter commands:");
                        break;
                }

                commandStringBuilder.AppendLine(Console.ReadLine()?.ToUpper());
                i++;
            }

            return commandStringBuilder.ToString();
        }
    }
}